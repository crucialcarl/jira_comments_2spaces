# Identifies JIRA comments with two spaces after a period. 
#





jira_comments_2spaces carl$ python jira_comment_2spaces.py

TEST-1110 user88 used two spaces after a period!
[ test.  this ]

TEST-1111 user38 used two spaces after a period!
[ document.  We ]

TEST-1112 user91 used two spaces after a period!
[ finalized.  Later ]